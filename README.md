********************************************************************
##     Lab L7 Load Balancing with Ingress (Nginx Controller)

********************************************************************
### What is Ingress?
- Ingress is an API Object that exposes HTTP and HTTPS routes from outside the cluster to services within the cluster. 
- Traffic routing is controlled by rules defined on the Ingress resource.


![Ingress Traffic](./img/ingress.png)

- An Ingress may be configured to give Services externally-reachable URLs, load balance traffic, terminate SSL / TLS, and offer name-based virtual hosting. 
- An Ingress controller is responsible for fulfilling the Ingress, usually with a load balancer, though it may also configure your edge router or additional frontends to help handle the traffic.
- An Ingress does not expose arbitrary ports or protocols. Exposing services other than HTTP and HTTPS to the internet typically uses a service of type Service.Type=NodePort or Service.Type=LoadBalancer

- See Documentations for more details [here](https://kubernetes.io/docs/concepts/services-networking/ingress/).


- **_Note:_**  All figures in this lab have been reproduced from the official Kubernetes documentation.
### Prerequisites

- You must have an Ingress controller to satisfy an Ingress. Only creating an Ingress resource has no effect.

- To satisfy an Ingress, you may need to deploy an [Ingress controller](https://kubernetes.io/docs/concepts/services-networking/ingress-controllers/) such as [ingress-nginx](https://github.com/kubernetes/ingress-nginx/blob/main/README.md#readme).

### Install Nginx Ingress Controller

- For more details about the installation Guide, you can visit the Nginx Documentation [here](https://kubernetes.github.io/ingress-nginx/deploy/#bare-metal).
#### Kubeadm Bare Metal Cluster (skip this step when using RKE cluster)

```
$ kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.0.0/deploy/static/provider/baremetal/deploy.yaml
namespace/ingress-nginx created
serviceaccount/ingress-nginx created
configmap/ingress-nginx-controller created
clusterrole.rbac.authorization.k8s.io/ingress-nginx unchanged
clusterrolebinding.rbac.authorization.k8s.io/ingress-nginx unchanged
role.rbac.authorization.k8s.io/ingress-nginx created
rolebinding.rbac.authorization.k8s.io/ingress-nginx created
service/ingress-nginx-controller-admission created
service/ingress-nginx-controller created
deployment.apps/ingress-nginx-controller created
ingressclass.networking.k8s.io/nginx unchanged
validatingwebhookconfiguration.admissionregistration.k8s.io/ingress-nginx-admission configured
serviceaccount/ingress-nginx-admission created
clusterrole.rbac.authorization.k8s.io/ingress-nginx-admission unchanged
clusterrolebinding.rbac.authorization.k8s.io/ingress-nginx-admission unchanged
role.rbac.authorization.k8s.io/ingress-nginx-admission created
rolebinding.rbac.authorization.k8s.io/ingress-nginx-admission created
job.batch/ingress-nginx-admission-create created
job.batch/ingress-nginx-admission-patch created


$ kubectl get pods -n ingress-nginx \
>   -l app.kubernetes.io/name=ingress-nginx --watch
NAME                                        READY   STATUS      RESTARTS   AGE
ingress-nginx-admission-create-nn47j        0/1     Completed   0          31s
ingress-nginx-admission-patch-4b9lm         0/1     Completed   1          31s
ingress-nginx-controller-57ffff5864-lhpzj   1/1     Running     0          32s


$ kubectl get all -n ingress-nginx
NAME                                            READY   STATUS      RESTARTS   AGE
pod/ingress-nginx-admission-create-nn47j        0/1     Completed   0          2m3s
pod/ingress-nginx-admission-patch-4b9lm         0/1     Completed   1          2m3s
pod/ingress-nginx-controller-57ffff5864-lhpzj   1/1     Running     0          2m4s

NAME                                         TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)                      AGE
service/ingress-nginx-controller             NodePort    10.104.73.236   <none>        80:31199/TCP,443:32630/TCP   2m4s
service/ingress-nginx-controller-admission   ClusterIP   10.96.123.89    <none>        443/TCP                      2m4s

NAME                                       READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/ingress-nginx-controller   1/1     1            1           2m4s

NAME                                                  DESIRED   CURRENT   READY   AGE
replicaset.apps/ingress-nginx-controller-57ffff5864   1         1         1       2m4s

NAME                                       COMPLETIONS   DURATION   AGE
job.batch/ingress-nginx-admission-create   1/1           2s         2m3s
job.batch/ingress-nginx-admission-patch    1/1           3s         2m3s


```
#### Rancher RKE Cluster (You have an Ingress Controller By Default)
- To verify installation run the following command:

```
ingress-demo$ kubectl get all -n ingress-nginx
NAME                                        READY   STATUS    RESTARTS   AGE
pod/default-http-backend-67cf578fc4-lthn9   1/1     Running   0          55m
pod/nginx-ingress-controller-vqxkr          1/1     Running   0          55m

NAME                           TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)   AGE
service/default-http-backend   ClusterIP   10.43.213.242   <none>        80/TCP    55m

NAME                                      DESIRED   CURRENT   READY   UP-TO-DATE   AVAILABLE   NODE SELECTOR   AGE
daemonset.apps/nginx-ingress-controller   1         1         1       1            1           <none>          55m

NAME                                   READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/default-http-backend   1/1     1            1           55m

NAME                                              DESIRED   CURRENT   READY   AGE
replicaset.apps/default-http-backend-67cf578fc4   1         1         1       55m
```

### Working with Ingress Rules
#### Using  Ingress with simple fanout

- Clone Application Repository

```
$ git clone https://gitlab.com/walidsaad/ingress-demo
$ cd ingress-demo/ingress
```
- Create ingress.yaml file
```
$ cat ingress.yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: ingress-example
  annotations:
    #kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  rules:
  - http:
      paths:
      - pathType: Prefix
        path: /bar
        backend:
          service:
            name: bar-app
            port:
              number: 3000

```

- Run Applications And Ingress

```
ingress-demo/ingress$ kubectl apply -f bar.yml
deployment.apps/bar-app created
```
- Expose Deployment to Ingress
```
ingress-demo/ingress$ kubectl expose deployment bar-app --type=NodePort --port=3000
service/bar-app exposed
ingress-demo/ingress$ kubectl apply -f ingress.yml
ingress.networking.k8s.io/ingress-example created
ingress-demo/ingress$ kubectl get all
NAME                           READY   STATUS    RESTARTS   AGE
pod/bar-app-6d46956f5f-glnlf   1/1     Running   0          94s
pod/bar-app-6d46956f5f-kf5pb   1/1     Running   0          94s

NAME                 TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
service/bar-app      NodePort    10.43.180.185   <none>        3000:31462/TCP   24s
service/kubernetes   ClusterIP   10.43.0.1       <none>        443/TCP          28m

NAME                      READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/bar-app   2/2     2            2           94s

NAME                                 DESIRED   CURRENT   READY   AGE
replicaset.apps/bar-app-6d46956f5f   2         2         2       94s


```
- Check Ingress Resource

```
ingress-demo/ingress$ kubectl get ingress
NAME              HOSTS   ADDRESS                     PORTS   AGE
ingress-example   *       master.kube   80      10m
ingress-demo/ingress$ kubectl describe ingress ingress-example
Name:             ingress-example
Namespace:        default
Address:          master.kube
Default backend:  default-http-backend:80 (<error: endpoints "default-http-backend" not found>)
Rules:
  Host        Path  Backends
  ----        ----  --------
  *
              /bar   bar-app:3000 (10.42.0.22:3000,10.42.0.23:3000)
Annotations:  field.cattle.io/publicEndpoints:
                [{"addresses":[""],"port":80,"protocol":"HTTP","serviceName":"default:bar-app","ingressName":"default:ingress-example","path":"/bar","allN...
              nginx.ingress.kubernetes.io/rewrite-target: /
Events:
  Type    Reason  Age                From                      Message
  ----    ------  ----               ----                      -------
  Normal  CREATE  11m                nginx-ingress-controller  Ingress default/ingress-example
  Normal  UPDATE  11m (x2 over 11m)  nginx-ingress-controller  Ingress default/ingress-example

```

- Testing Endpoint Out

```
ingress-demo$ curl -kL http://localhost/bar
Hello World from  BAR
ingress-demo$ curl -kL http://@IP/bar
Hello World from  BAR
```

- **_Note:_** Traffic that is sent to /bar will be directed to the service bar-app on port 3000. This is a basic setup that provides OSI layer 7 functionality.

- **_Note:_** For Kubeadm Cluster, you should add the annotation ` kubernetes.io/ingress.class: nginx` to your `ingress.yml` file.

- **_Note:_** For Kubeadm Cluster, you can use these commands `curl http://@IP:NodePort/bar` and  `curl http://@IP:NodePort/foo`

#### Using  Ingress to load balance traffic
- Now we can add another path. This allows us to load balance between different backends for our application. We can split up traffic and send it to different service endpoints and deployments based on the path.

![Ingress Traffic](./img/ingress2.png)

- Run Applications And Update Ingress (add /foo path)

```
ingress-demo/ingress$ kubectl apply -f foo.yml
deployment.apps/foo-app created
ingress-demo/ingress$ cat ingress.yml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: ingress-example
  annotations:
    #kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  rules:
  - http:
      paths:
      - pathType: Prefix
        path: /bar
        backend:
          service:
            name: bar-app
            port:
              number: 3000
      - pathType: Prefix
        path: /foo
        backend:
          service:
            name: foo-app
            port:
              number: 3000


```
- Expose Deployment to Ingress

```
ingress-demo/ingress$ kubectl expose deployment foo-app --type=NodePort --port=3000
service/foo-app exposed

ingress-demo/ingress$ kubectl get all
NAME                           READY   STATUS    RESTARTS   AGE
pod/bar-app-6d46956f5f-glnlf   1/1     Running   0          34m
pod/bar-app-6d46956f5f-kf5pb   1/1     Running   0          34m
pod/foo-app-846b9546d8-hxgp6   1/1     Running   0          4m25s
pod/foo-app-846b9546d8-vfmjh   1/1     Running   0          4m25s

NAME                 TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
service/bar-app      NodePort    10.43.180.185   <none>        3000:31462/TCP   33m
service/foo-app      NodePort    10.43.138.44    <none>        3000:31325/TCP   3m52s
service/kubernetes   ClusterIP   10.43.0.1       <none>        443/TCP          61m

NAME                      READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/bar-app   2/2     2            2           34m
deployment.apps/foo-app   2/2     2            2           4m25s

NAME                                 DESIRED   CURRENT   READY   AGE
replicaset.apps/bar-app-6d46956f5f   2         2         2       34m
replicaset.apps/foo-app-846b9546d8   2         2         2       4m25s


```
- Check Ingress Resource

```
ingress-demo/ingress$ kubectl apply -f ingress.yml
ingress.networking.k8s.io/ingress-example configured

ingress-demo/ingress$ kubectl get ingress
NAME              HOSTS   ADDRESS                     PORTS   AGE
ingress-example   *       master.kube   80      31m
ingress-demo/ingress$ kubectl describe ingress ingress-example
Name:             ingress-example
Namespace:        default
Address:         master.kube
Default backend:  default-http-backend:80 (<error: endpoints "default-http-backend" not found>)
Rules:
  Host        Path  Backends
  ----        ----  --------
  *
              /bar   bar-app:3000 (10.42.0.22:3000,10.42.0.23:3000)
              /foo   foo-app:3000 (10.42.0.24:3000,10.42.0.25:3000)
Annotations:  field.cattle.io/publicEndpoints:
                [{"addresses":[""],"port":80,"protocol":"HTTP","serviceName":"default:bar-app","ingressName":"default:ingress-example","path":"/bar","allN...
              nginx.ingress.kubernetes.io/rewrite-target: /
Events:
  Type    Reason  Age                  From                      Message
  ----    ------  ----                 ----                      -------
  Normal  CREATE  31m                  nginx-ingress-controller  Ingress default/ingress-example
  Normal  UPDATE  2m23s (x4 over 31m)  nginx-ingress-controller  Ingress default/ingress-example

```
- Testing Endpoint Out

```
ingress-demo$ curl -kL http://localhost/bar
Hello World from  BAR
ingress-demo$ curl -kL http://localhost/foo
Hello World from  FOO
```

 - **_Note:_** Using multiple path can be beneficial for endpoints that receive more traffic, as we can scale a single deployment for /bar without having to scale up /foo.
#### Using  Ingress Name based virtual hosting 
- We can use Kubernetes Ingress to route HTTP traffic to multiple hostnames at the same IP address.
- With Cloud Providers like AKS, EKS or GKE, multiple DNS records can be pointed at the same public IP address used for the LoadBalancer service of our Ingress Controller.
- So, we can use a different ingress resource per host, which allows us to control traffic with multiple hostnames, while using the same external IP for A Record creation. 

- Traffic that is sent to bar.com/bar will be directed to the service bar-app on port 3000.
- Traffic that is sent to goo.com/bar will be directed to the service foo-app on port 3000.

![Ingress Traffic](./img/ingress3.png)


- Start by updating the ingress resource file :
```
ingress-demo/ingress$ cat ingress.yml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: ingress-example
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  rules:
  - host: bar.com
    http:
      paths:
      - path: /bar
        pathType: Prefix
        backend:
          service:
              name:  bar-app
              port: 
                number: 3000
  - host: foo.com
    http:
       paths:
       - path: /foo
         pathType: Prefix
         backend:
          service:
              name: foo-app
              port: 
                number: 3000

```
- Configure again  Ingress

```
ingress-demo/ingress$ kubectl apply -f ingress.yml
ingress.networking.k8s.io/ingress-example configured
rancher@taurus-1:/home/rancher/ingress-demo/ingress$ kubectl describe ingress ingress-example
Name:             ingress-example
Namespace:        default
Address:          taurus-1.lyon.grid5000.fr
Default backend:  default-http-backend:80 (<error: endpoints "default-http-backend" not found>)
Rules:
  Host        Path  Backends
  ----        ----  --------
  bar.com
              /bar   bar-app:3000 (10.42.0.28:3000,10.42.0.29:3000)
  foo.com
              /foo   foo-app:3000 (10.42.0.27:3000,10.42.0.30:3000)
Annotations:  field.cattle.io/publicEndpoints:
                [{"addresses":[""],"port":80,"protocol":"HTTP","serviceName":"default:bar-app","ingressName":"default:ingress-example","hostname":"bar.com...
              nginx.ingress.kubernetes.io/rewrite-target: /
Events:
  Type    Reason  Age               From                      Message
  ----    ------  ----              ----                      -------
  Normal  UPDATE  5s (x8 over 97m)  nginx-ingress-controller  Ingress default/ingress-example

```

- For local testing purpose, you should add these ligne to your hosts file :
```
ingress-demo/ingress$ cat /etc/hosts
127.0.0.1       localhost bar.com foo.com
127.0.1.1       localhost.localdomain   localhost

```

- Test Ingress
```
ingress-demo/ingress$ curl http://bar.com/bar
Hello World from  BAR
ingress-demo/ingress$ curl http://foo.com/foo
Hello World from  FOO
```

#### More Advanced Scenario

```
ingress-demo/ingress$ cat ingress-test-prod.yml
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: ingress-example
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  rules:
  - host: test.foobar.com
    http:
      paths:
      - path: /bar
        backend:
          serviceName: test-bar-app
          servicePort: 3000
      - path: /foo
         backend:
          serviceName: test-foo-app
          servicePort: 3000

  - host: prod.foobar.com
    http:
       paths:
       - path: /bar
         backend:
          serviceName: prod-bar-app
          servicePort: 3000
       - path: /foo
         backend:
          serviceName: prod-foo-app
          servicePort: 3000

```
#### Using Ingress to terminate SSL / TLS

- In practice  we really need set up services by using HTTPS. 
- To do this we need to install a Kubernetes Custom Resource Definition called [Cert-Manager](https://artifacthub.io/packages/helm/cert-manager/cert-manager). 
- Cert-Manager automates TLS certificate management using multiple providers.

- Once we have installed Cert-Manager we can create certificates with annotations in the Ingress Resource, or [manually](https://kubernetes.io/docs/concepts/services-networking/ingress/) by creating our own Certificates.
- In the example bellow we use annotations as it simplifies the configuration :

```
ingress-demo/ingress$ cat ingress-tls.yml
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
 name: foo-bar
 annotations:
   nginx.ingress.kubernetes.io/rewrite-target: /
   nginx.ingress.kubernetes.io/force-ssl-redirect: "true"
   cert-manager.io/cluster-issuer: letsencrypt-prod
spec:
 tls:
 - hosts:
   - prod.foobar.com
   secretName: foobar-tls
 rules:
 - host: prod.foobar.com
   http:
     paths:
     - path: /bar
         backend:
          serviceName: prod-bar-app
          servicePort: 3000
     - path: /foo
         backend:
          serviceName: prod-foo-app
          servicePort: 3000

```

 - **_Note:_**  Nginx provides options that can be configured using annotations. 
 - We can use `force-ssl-redirect` to redirect HTTP traffic to HTTPS.
 - The Cert-Manager annotation `cluster-issuer` lets us select which issuer we want to use for our certificate.  
 - A secret name is selected to store the certificate information which is automatically configured since we are using a Cert-Manager annotation.
